import logo from './logo.svg';
import { Routes, Route, Link } from "react-router-dom";
import './App.css';
import Home from "./pages/Home";
// import About from "./pages/About";
function App() {
  return (
      <div className="App">
          <Routes>
              <Route path="/" element={<Home />} />
              <Route path="about" element={<About />} />
          </Routes>
      </div>
  );
}

function About() {
    return (
        <div>
            about
        </div>
    );
}

export default App;
