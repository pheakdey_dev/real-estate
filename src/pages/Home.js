import React, { useState } from 'react';

import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import PinDropIcon from '@mui/icons-material/PinDrop';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import OutlinedInput from '@mui/material/OutlinedInput';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions } from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';

import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';
import IconButton from '@mui/material/IconButton';

import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import HttpsIcon from '@mui/icons-material/Https';

const backgroundImage = 'https://previews.123rf.com/images/guijunpeng/guijunpeng1201/guijunpeng120100014/12156372-la-luz-de-los-senderos-en-el-fondo-moderno-edificio-en-shangai-china-.jpg';
const infinityLogo = '/infinity-logo.png';

function Home() {
    const [showPassword, setShowPassword] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation();

        alert('Submitted')
    };

    const handleClickShowPassword = () => {
        setShowPassword(prev => !prev)
    };

    return (
        <>
            <Container maxWidth="lg">

                <Box onSubmit={(e) => onSubmit(e)}
                     sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    p: 1,
                    m: 1,
                    bgcolor: 'background.paper',
                    borderRadius: 1
                }}>
                    {/*Left Card*/}
                    <Card sx={{ maxWidth: 345 }}>
                        <CardContent>
                            <div style={{width: 280, padding: 30}}>
                                <h3 style={{
                                    fontSize: '1.2em',
                                    color: '#646464',
                                    fontWeight: 500
                                }}>WELCOME TO</h3>
                                <CardMedia
                                    component="img"
                                    image={infinityLogo}
                                    alt="green iguana"
                                    style={{
                                        width: '100%',
                                        maxWidth: 180,
                                        height: 'auto',
                                        margin: '0 auto'
                                    }}
                                />
                                <p style={{
                                    fontSize: '0.8em',
                                    color: 'gray'
                                }}>Log in to get the moment updates on the things<br /> that interest you.</p>
                            </div>

                            <Box
                                component="form"
                                sx={{
                                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                                }}
                                noValidate
                                autoComplete="off"
                            >
                                {/*Username*/}
                                <FormControl sx={{ m: 1, width: '100%' }} variant="outlined">
                                    <OutlinedInput
                                        id="outlined-adornment-username"
                                        required
                                        startAdornment={<InputAdornment position="start"><PersonIcon /></InputAdornment>}
                                        label="Username"
                                    />
                                    <InputLabel htmlFor="outlined-adornment-username">Username</InputLabel>
                                </FormControl>

                                {/*Password*/}
                                <FormControl sx={{ m: 1, width: '100%' }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-password"
                                        required
                                        type={showPassword ? 'text' : 'password'}
                                        startAdornment={
                                            <InputAdornment position="start">
                                                <HttpsIcon />
                                            </InputAdornment>
                                        }
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                </FormControl>

                                <FormControl sx={{ m: 1, width: '100%' }}>
                                    <Button type="submit" variant="contained">SING IN</Button>
                                </FormControl>
                            </Box>

                            <p style={{
                                fontSize: '0.8em',
                                marginTop: '10px',
                                marginBottom: 0,
                            }}>Don't have an account? <span style={{
                                color: '#fd6b56',
                                fontWeight: 800,
                                fontSize: '0.9em'
                            }}>Sign Up Now</span></p>

                        </CardContent>

                        <CardActions>
                            <div style={{
                                width: '100%',
                            }}>
                                <p style={{
                                    marginTop: 0,
                                    fontSize:'0.8em',
                                }}>Continue with social media</p>

                                <div className="socialMedia">
                                    <FacebookIcon/>
                                    <TwitterIcon/>
                                    <YouTubeIcon/>
                                </div>
                            </div>
                        </CardActions>
                    </Card>

                    {/*Right Card*/}
                    <Card sx={{maxWidth: 345, display: 'flex'}}>
                        <div style={{
                            backgroundImage: `url(${backgroundImage})`,
                            backgroundSize: 'cover',
                            position: 'relative',
                            display: 'flex'
                        }}>
                            <div style={{backgroundColor: 'red', width: '100%', height: '100%', opacity: 0.5, position: 'absolute', zIndex: 1}}></div>
                            <CardActionArea style={{
                                position: 'relative',
                                zIndex: 1
                            }}>
                                <CardContent>
                                    <PinDropIcon style={{color: 'white'}} />
                                    <Typography gutterBottom
                                                variant="h5"
                                                component="div"
                                                style={{
                                                    color: 'white'
                                                }}
                                    >
                                        INFINTY
                                    </Typography>
                                    <Typography variant="body2"
                                                style={{
                                                    color: '#f5dddd',
                                                    fontSize: 10
                                                }}
                                    >
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum facilisis quam ac neque suscipit commodo. Mauris condimentum sem ligula,
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </div>
                    </Card>
                </Box>

            </Container>
        </>
    );
}
export default Home;
